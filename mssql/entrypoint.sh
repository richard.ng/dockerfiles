#!/bin/bash
set -e

if [ ! -f /var/opt/mssql/db-initialized ]; then
  function initialize_db() {
    sleep 15s

    /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "Password1!" -i setup.sql
    /opt/mssql-tools/bin/bcp HeroValue in "heroes.csv" -c -t, -S localhost -U SA -P "Password1!" -d heroes

    touch /var/opt/mssql/db-initialized
  }
  initialize_db &
fi

exec "$@"
